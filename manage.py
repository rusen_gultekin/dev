from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from os import environ
from sys import exit
import unittest

from config import config_dict
from app import create_app, db

get_config_mode = environ.get('APP_CONFIG_MODE', 'Debug')

try:
    config_mode = config_dict[get_config_mode.capitalize()]
except KeyError:
    exit('Error: Invalid APP_CONFIG_MODE environment variable entry.')

app = create_app(config_mode)
manager = Manager(app)
Migrate(app, db)
manager.add_command('db', MigrateCommand)

@manager.command
def run():
    app.run()

@manager.command
def test():
    """Runs the unit tests."""
    tests = unittest.TestLoader().discover('test', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1

if __name__ == "__main__":
    manager.run()
