import unittest
import json
from test.base import BaseTestCase
from flask_api import status


class TestAudienceServices(BaseTestCase):

    def test_audience_list(self):
        response = self.client.get('/api/v1/audience')
        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_audience_create(self):
        payload = {
            "name": "test 999999",
            "description": "desc 33",
            "vodplans": ["plan1", "plan2", "plan3"]

        }
        response = self.client.post('/api/v1/audience', json=payload)
        data = json.loads(response.data.decode())

        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(data['audience'].get('id'))

        #delete created data
        response = self.client.delete('/api/v1/audience/'+str(data['audience'].get('id')))
        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)



    def test_audience_create_without_payload(self):
        payload = None
        response = self.client.post('/api/v1/audience', json=payload)

        self.assertEqual(response.status_code,status.HTTP_400_BAD_REQUEST)


    def test_audience_create_without_value(self):
        payload = {}
        response = self.client.post('/api/v1/audience', json=payload)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_audience_detail(self):
        #create audience
        payload = {
            "name": "test 999999",
            "description": "desc 33",
            "vodplans": ["plan1", "plan2", "plan3"]

        }
        response = self.client.post('/api/v1/audience', json=payload)
        data = json.loads(response.data.decode())

        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(data['audience'].get('id'))

        #get detail
        response = self.client.get('/api/v1/audience/'+str(data['audience'].get('id')))
        self.assertTrue(response)
        self.assertEqual(response.status_code,status.HTTP_200_OK)

        # delete created data
        response = self.client.delete('/api/v1/audience/' + str(data['audience'].get('id')))
        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_audience_detail_with_invalid_id(self):
        response=self.client.get('/api/v1/audience/123456789')
        self.assertEqual(response.status_code,status.HTTP_404_NOT_FOUND)



    def test_audience_update(self):
        # create audience
        payload = {
            "name": "test 999999",
            "description": "desc 33",
            "vodplans": ["plan1", "plan2", "plan3"]

        }
        response = self.client.post('/api/v1/audience', json=payload)
        data = json.loads(response.data.decode())

        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(data['audience'].get('id'))

        #update audience
        payload = {
            "name": "test 88888",
            "description": "desc 33",
            "vodplans": ["plan1", "plan2", "plan3"]

        }

        response = self.client.put('/api/v1/audience/'+str(data['audience'].get('id')),json=payload)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(data["audience"].get("name"),"test 88888")


        # delete created data
        response = self.client.delete('/api/v1/audience/' + str(data['audience'].get('id')))
        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_audience_update_with_invalid_id(self):
        payload = {
            "name": "test 88888",
            "description": "desc 33",
            "vodplans": ["plan1", "plan2", "plan3"]

        }
        response = self.client.put('/api/v1/audience/123456789',json=payload)
        self.assertEqual(response.status_code,status.HTTP_404_NOT_FOUND)

    def test_audience_update_without_payload(self):
        # create audience
        payload = {
            "name": "test 999999",
            "description": "desc 33",
            "vodplans": ["plan1", "plan2", "plan3"]

        }
        response = self.client.post('/api/v1/audience', json=payload)
        data = json.loads(response.data.decode())

        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(data['audience'].get('id'))

        #update
        payload = None
        response = self.client.put('/api/v1/audience/'+str(data['audience'].get('id')),json=payload)
        self.assertEqual(response.status_code,status.HTTP_400_BAD_REQUEST)

        # delete created data
        response = self.client.delete('/api/v1/audience/' + str(data['audience'].get('id')))
        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_audience_update_without_value(self):
        # create audience
        payload = {
            "name": "test 999999",
            "description": "desc 33",
            "vodplans": ["plan1", "plan2", "plan3"]

        }
        response = self.client.post('/api/v1/audience', json=payload)
        data = json.loads(response.data.decode())

        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(data['audience'].get('id'))

        #update
        payload = {}
        response=self.client.put('/api/v1/audience/'+str(data['audience'].get('id')),json=payload)
        self.assertEqual(response.status_code,status.HTTP_400_BAD_REQUEST)

        # delete created data
        response = self.client.delete('/api/v1/audience/' + str(data['audience'].get('id')))
        self.assertTrue(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)



if __name__ == '__main__':
    unittest.main()
