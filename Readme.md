### Installation
- python3 -m venv venv
- source venv/bin/activate
- pip install -r requirements.txt

### to run project
- python manage.py run

- view on browser url : http://127.0.0.1:5000/
    
### to run test cases
- python manage.py test