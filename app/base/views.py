from flask import jsonify, render_template, redirect, request, url_for, abort
from app import db
from app.base import blueprint
from app.base.models import Audience, Location, Assets
from app.base.serializer import audience_schema, audiences_schema, \
    location_schema, locations_schema, asset_schema, assets_schema
from marshmallow import ValidationError


##audience views
@blueprint.route('/api/v1/audience', methods=['GET'])
def audience_list():
    audiences = Audience.query.all()
    results = audiences_schema.dump(audiences)

    return jsonify({"audiences": results}), 200


@blueprint.route('/api/v1/audience/<int:audience_id>', methods=['GET'])
def audience_detail(audience_id):
    audience = Audience.query.get(audience_id)
    if not audience:
        return jsonify({'message': "Audience could not be found"}), 404
    result = audience_schema.dump(audience)

    return jsonify({'audience': result}), 200


@blueprint.route('/api/v1/audience/<int:audience_id>', methods=['DELETE'])
def audience_delete(audience_id):
    audience = Audience.query.get(audience_id)
    if not audience:
        return jsonify({'message': "Audience could not be found"}), 404

    db.session.delete(audience)
    db.session.commit()
    return jsonify({'status': 'success'}), 200


@blueprint.route('/api/v1/audience', methods=['POST'])
def audience_create():
    json_data = request.get_json(force=True)
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400

    try:
        data = audience_schema.load(json_data)
    except ValidationError as err:
        return err.messages, 422

    audience = Audience.query.filter_by(name=data['name']).first()
    if audience:
        return jsonify({'message': 'Audience already exists'}), 400

    audience = Audience(name=data['name'], description=data['description'], vodplans=data['vodplans'])
    db.session.add(audience)
    db.session.commit()
    result = audience_schema.dump(Audience.query.get(audience.id))

    return jsonify({'status': 'success', 'audience': result}), 201


@blueprint.route('/api/v1/audience/<int:audience_id>', methods=['PUT'])
def audience_update(audience_id):
    json_data = request.get_json(force=True)
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400

    audience = Audience.query.get(audience_id)
    if not audience:
        return jsonify({'message': "Audience could not be found"}), 404

    try:
        data = audience_schema.load(json_data)
    except ValidationError as err:
        return err.messages, 422

    audience.name = data.get("name")
    audience.description = data.get("description")
    audience.vodplans = data.get("vodplans")
    db.session.commit()

    result = audience_schema.dump(audience)

    return jsonify({'status': 'success', 'audience': result}), 200


##location views
@blueprint.route('/api/v1/location', methods=['GET'])
def location_list():
    locations = Location.query.all()
    results = locations_schema.dump(locations)

    return jsonify({"locations": results}), 200


@blueprint.route('/api/v1/location/<int:location_id>', methods=['GET'])
def location_detail(location_id):
    location = Location.query.get(location_id)
    if not location:
        return jsonify({'message': "Location could not be found"}), 404
    result = location_schema.dump(location)

    return jsonify({'location': result}), 200


@blueprint.route('/api/v1/location/<int:location_id>', methods=['DELETE'])
def location_delete(location_id):
    location = Location.query.get(location_id)
    if not location:
        return jsonify({'message': "Location could not be found"}), 404

    db.session.delete(location)
    db.session.commit()
    return jsonify({'status': 'success'}), 200


@blueprint.route('/api/v1/location', methods=['POST'])
def location_create():
    json_data = request.get_json(force=True)
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400

    try:
        data = location_schema.load(json_data)
    except ValidationError as err:
        return err.messages, 422

    location = Location.query.filter_by(name=data['name']).first()
    if location:
        return jsonify({'message': 'Location already exists'}), 400

    audience = Audience.query.filter_by(name=data['audience']["name"]).first()
    if audience is None:
        return jsonify({'message': 'Audience type could not be found'}), 404

    location = Location(
        name=data['name'],
        description=data['description'],
        installation_date=data['installation_date'],
        live_date=data['live_date'],
        distribution_method=data['distribution_method'],
        audience=audience
    )

    db.session.add(location)
    db.session.commit()
    result = location_schema.dump(Location.query.get(location.id))

    return jsonify({'status': 'success', 'location': result}), 201


@blueprint.route('/api/v1/location/<int:location_id>', methods=['PUT'])
def location_update(location_id):
    json_data = request.get_json(force=True)
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400

    location = Location.query.get(location_id)
    if not location:
        return jsonify({'message': "Location could not be found"}), 404

    try:
        data = location_schema.load(json_data)
    except ValidationError as err:
        return err.messages, 422

    audience = Audience.query.filter_by(name=data['audience']["name"]).first()
    if audience is None:
        return jsonify({'message': 'Audience type could not be found'}), 404

    location.name = data.get("name")
    location.description = data.get("description")
    location.installation_date = data.get("installation_date")
    location.live_date = data.get("live_date")
    location.distribution_method = data.get("distribution_method")
    location.audience = audience

    db.session.commit()

    result = location_schema.dump(location)

    return jsonify({'status': 'success', 'location': result}), 200


##assets views
@blueprint.route('/api/v1/assets', methods=['GET'])
def assets_list():
    assets = Assets.query.all()
    results = assets_schema.dump(assets)

    return jsonify({"assets": results}), 200


@blueprint.route('/api/v1/assets/<int:asset_id>', methods=['GET'])
def asset_detail(asset_id):
    asset = Assets.query.get(asset_id)
    if not asset:
        return jsonify({'message': "Asset could not be found"}), 404
    result = asset_schema.dump(asset)

    return jsonify({'asset': result}), 200


@blueprint.route('/api/v1/assets/<int:asset_id>', methods=['DELETE'])
def asset_delete(asset_id):
    asset = Assets.query.get(asset_id)
    if not asset:
        return jsonify({'message': "Asset could not be found"}), 404

    db.session.delete(asset)
    db.session.commit()
    return jsonify({'status': 'success'}), 200


@blueprint.route('/api/v1/assets', methods=['POST'])
def asset_create():
    json_data = request.get_json(force=True)
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400

    try:
        data = asset_schema.load(json_data)
    except ValidationError as err:
        return err.messages, 422

    asset = Assets.query.filter_by(asset_no=data['asset_no']).first()
    if asset:
        return jsonify({'message': 'Asset already exists'}), 400

    location = Location.query.filter_by(name=data['location']["name"]).first()
    if location is None:
        return jsonify({'message': 'Location could not be found'}), 404

    asset = Assets(
        name=data['name'],
        description=data['description'],
        mapping_name=data['mapping_name'],
        serial_no=data['serial_no'],
        asset_no=data['asset_no'],
        manufacturer=data['manufacturer'],
        ip_address=data['ip_address'],
        hdd_capacity=data['hdd_capacity'],
        hdd_layout=data['hdd_layout'],
        location=location
    )

    db.session.add(asset)
    db.session.commit()
    result = asset_schema.dump(Assets.query.get(asset.id))

    return jsonify({'status': 'success', 'asset': result}), 201


@blueprint.route('/api/v1/assets/<int:asset_id>', methods=['PUT'])
def asset_update(asset_id):
    json_data = request.get_json(force=True)
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400

    asset = Assets.query.get(asset_id)
    if not asset:
        return jsonify({'message': "Asset could not be found"}), 404

    try:
        data = asset_schema.load(json_data)
    except ValidationError as err:
        return err.messages, 422

    location = Location.query.filter_by(name=data['location']["name"]).first()
    if location is None:
        return jsonify({'message': 'Location could not be found'}), 404

    asset.name = data.get("name")
    asset.description = data.get("description")
    asset.mapping_name = data.get("mapping_name")
    asset.serial_no = data.get("serial_no")
    asset.asset_no = data.get("asset_no")
    asset.manufacturer = data.get("manufacturer")
    asset.ip_address = data.get("ip_address")
    asset.hdd_capacity = data.get("hdd_capacity")
    asset.hdd_layout = data.get("hdd_layout")
    asset.location = location

    db.session.commit()

    result = asset_schema.dump(asset)

    return jsonify({'status': 'success', 'asset': result}), 200
