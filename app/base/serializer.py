from app import ma
from app.base.models import Audience, Location, Assets
from marshmallow import fields


class AudienceSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Audience


class LocationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Location
    audience = fields.Nested(AudienceSchema, only=["name"], required=True)


class AssetSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Assets
    location = fields.Nested(LocationSchema, only=["name"], required=True)


audience_schema = AudienceSchema()
audiences_schema = AudienceSchema(many=True)

location_schema = LocationSchema()
locations_schema = LocationSchema(many=True)

asset_schema = AssetSchema()
assets_schema = AssetSchema(many=True)
