from app import db


class Audience(db.Model):
    __tablename__ = 'audience'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(250), nullable=False)
    vodplans = db.Column(db.ARRAY(db.Text()), nullable=False)

    def __repr__(self):
        return '<Audience %r>' % self.name


class Location(db.Model):
    __tablename__ = 'location'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(250))
    installation_date = db.Column(db.DateTime)
    live_date = db.Column(db.DateTime)
    distribution_method = db.Column(db.String(100))
    audience_type = db.Column(db.Integer, db.ForeignKey('audience.id'), nullable=False)
    audience = db.relationship('Audience', backref=db.backref('locations', lazy=True))

    def __repr__(self):
        return '<Location %r>' % self.name

class Assets(db.Model):
    __tablename__ = 'assets'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    mapping_name = db.Column(db.String(100))
    serial_no = db.Column(db.String(50), nullable=False)
    asset_no = db.Column(db.String(50), nullable=False)
    manufacturer = db.Column(db.String(50))
    ip_address = db.Column(db.ARRAY(db.JSON()), nullable=False)
    hdd_capacity = db.Column(db.String(50))
    hdd_layout = db.Column(db.String(50))
    description = db.Column(db.String(250))
    location_id = db.Column(db.Integer, db.ForeignKey('location.id'), nullable=False)
    location = db.relationship('Location', backref=db.backref('assets', lazy=True))

    def __repr__(self):
        return '<Assets %r>' % self.name
