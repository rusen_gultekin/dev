import os
from os import environ
from dotenv import load_dotenv
load_dotenv('.env')

class Config(object):
    basedir = os.path.abspath(os.path.dirname(__file__))

    SECRET_KEY = 'XYZDEFLRT'
    SQLALCHEMY_DATABASE_URI = 'postgresql://{}:{}@{}:{}/{}'.format(
        environ.get('APP_DATABASE_USER', 'appseed'),
        environ.get('APP_DATABASE_PASSWORD', 'appseed'),
        environ.get('APP_DATABASE_HOST', 'db'),
        environ.get('APP_DATABASE_PORT', 5432),
        environ.get('APP_DATABASE_NAME', 'appseed')
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    DEBUG = False


class DebugConfig(Config):
    DEBUG = True


class TestConfig(Config):
    DEBUG = True
    TESTING = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False


config_dict = {
    'Production': ProductionConfig,
    'Debug': DebugConfig,
    'Test': TestConfig
}
